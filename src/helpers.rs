pub mod helpers {
    use std::fs;

    pub fn file_to_vec_i32(filename: &str) -> Vec<i32> {
        let contents = fs::read_to_string(filename)
            .expect("couldn't read filsre");

        let splits = contents
            .split_whitespace()
            .map(|x| { x.to_string().parse::<i32>().unwrap() })
            .collect();

        return splits;
    }

    pub fn file_to_vec_i64(filename: &str) -> Vec<i64> {
        let contents = fs::read_to_string(filename)
            .expect("couldn't read filsre");

        let splits = contents
            .split_whitespace()
            .map(|x| { x.to_string().parse::<i64>().unwrap() })
            .collect();

        return splits;
    }

    pub fn file_to_vec_string(filename: &str) -> Vec<String> {
        let contents = fs::read_to_string(filename)
            .expect("couldn't read filsre");

        let splits = contents
            .split_terminator('\n')
            .map(|x| x.to_string())
            .collect::<Vec<_>>();

        return splits;
    }

    pub fn file_to_string(filename: &str) -> String {
        let contents = fs::read_to_string(filename)
            .expect("couldn't read file");

        return contents;
    }
}

#[cfg(test)]
mod tests {
    use super::helpers::*;

    #[test]
    fn file_to_vec_i32_readfromfile() {
        let list = file_to_vec_i32("inputs/p1.txt");

        assert_eq!(list.len(), 200);
    }

    #[test]
    fn file_to_vec_str_readsfromfile() {
        let list = file_to_vec_string("inputs/p2.txt");

        assert_eq!(list.len(), 1000);
    }
}