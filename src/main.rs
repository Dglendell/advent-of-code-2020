#[macro_use] extern crate lazy_static;

mod p1;
mod helpers;
mod p2;
mod p3;
mod p4;
mod passport;
mod p5;
mod p6;
mod p7;
mod p8;
mod p9;
mod p10;
mod p11;
mod p12;
mod p12_2;
mod p13;
mod p14;
mod p15;
mod p16;
mod p17;

fn main() {
    println!("Hello, world!");
}
