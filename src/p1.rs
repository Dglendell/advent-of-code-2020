mod p1 {
    pub fn find_sum_two(sum: i32, list: Vec<i32>) -> (i32, i32) {
        let mut list_copy = Vec::from(list);

        for _ in 0..list_copy.len() {
            let a = list_copy.pop().unwrap();

            for b in &list_copy {
                if a + b == sum {
                    return (a, *b);
                }
            }
        }

        return (-1, -1);
    }

    pub fn find_sum_three(sum: i32, list: Vec<i32>) -> (i32, i32, i32) {
        let mut list_copy = Vec::from(list);

        for _ in 0..list_copy.len() {
            let a = list_copy.pop().unwrap();

            for b in &list_copy {
                for c in &list_copy {
                    if b == c {
                        continue;
                    }

                    if a + b + c == sum {
                        return (a, *b, *c);
                    }
                }
            }
        }
        return (-1, -1, -1);
    }
}

#[cfg(test)]
mod tests {
    use crate::helpers::helpers::file_to_vec_i32;
    use crate::p1::p1::{find_sum_two, find_sum_three};

    #[test]
    fn test_find_sum() {
        let mut list : Vec<i32> = Vec::new();
        list.push(1721);
        list.push(979);
        list.push(366);
        list.push(299);
        list.push(675);
        list.push(1456);

        let (b, a) = find_sum_two(2020, list);
        assert_eq!(a, 1721);
        assert_eq!(b, 299);
        assert_eq!(a + b, 2020);
    }

    #[test]
    fn test_find_result() {
        let mut list : Vec<i32> = Vec::new();
        list.push(1721);
        list.push(979);
        list.push(366);
        list.push(299);
        list.push(675);
        list.push(1456);

        let (a, b) = find_sum_two(2020, list);
        assert_eq!(a * b, 514579);
    }

    #[test]
    fn test_find_result_part2() {
        let mut list : Vec<i32> = Vec::new();
        list.push(1721);
        list.push(979);
        list.push(366);
        list.push(299);
        list.push(675);
        list.push(1456);

        let (a, b, c) = find_sum_three(2020, list);
        assert_eq!(b, 979);
        assert_eq!(c, 366);
        assert_eq!(a, 675);
    }

    #[test]
    fn test_final_part1() {
        let values = file_to_vec_i32("inputs/p1.txt");

        let (a, b) = find_sum_two(2020, values);
        println!("The answer is {}", a * b);
    }

    #[test]
    fn test_final_part2() {
        let values = file_to_vec_i32("inputs/p1.txt");
        let (a, b, c) = find_sum_three(2020, values);
        println!("The answer is {}", a * b * c);
    }
}