use std::collections::HashMap;

fn find_differences(input: &Vec<i32>) -> [i32;3] {
    let mut sorted_input = input.clone();
    sorted_input.sort();

    let mut differences: [i32; 3] = [0, 0, 1];

    for (index, current) in sorted_input.iter().enumerate() {
        if index == 0 {
            differences[(*current - 1) as usize] += 1;
            continue;
        }

        if let Some(prev) = sorted_input.get(index - 1) {
            let difference = (*current - *prev) as usize;
            differences[difference - 1] += 1;
        }
    }

    differences
}

fn count_within_range(input: &Vec<i64>, cache: &mut HashMap<usize, u64>, index: usize) -> u64 {
    if index == input.len() - 1 {
        return 1;
    }
    if cache.contains_key(&index) {
        return cache.get(&index).unwrap().clone();
    }

    let right = input.get(index).unwrap().clone();
    let mut answer = 0;
    for j in (index + 1)..input.len() {
        let left = input.get(j).unwrap().clone();

        if (left - right) <= 3 {
            answer += count_within_range(input, cache, j);
        }
    }
    cache.insert(index, answer.clone());
    answer
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::helpers::{file_to_vec_i32, file_to_vec_i64};

    #[test]
    fn test_find_differences() {
        let input = vec![
            16,
            10,
            15,
            5,
            1,
            11,
            7,
            19,
            6,
            12,
            4];

        let answer = find_differences(&input);
        println!("{:?}", answer);
        assert_eq!(answer, [7, 0, 5]);
    }

    #[test]
    fn final_solution_part1() {
        let input = file_to_vec_i32("inputs/p10.txt");

        let answer = find_differences(&input);

        println!("{:?}", answer[0] * answer[2]);
    }

    #[test]
    fn test_count_within_range() {
        let mut input = vec![
            28,
33,
18,
42,
31,
14,
46,
20,
48,
47,
24,
23,
49,
45,
19,
38,
39,
11,
1,
32,
25,
35,
8,
17,
7,
9,
4,
2,
34,
10,
3
        ];
        input.insert(0, 0);
        input.insert(0, *input.iter().max().unwrap() + 3);
        input.sort();

        let mut cache: HashMap<usize, u64> = HashMap::new();
        let answer = count_within_range(&input, &mut cache, 0);
        assert_eq!(answer, 19208);
    }

    #[test]
    fn final_solution_part2() {
        let mut input = file_to_vec_i64("inputs/p10.txt");
        input.insert(0, 0);
        input.insert(0, *input.iter().max().unwrap() + 3);
        input.sort();

        let mut cache: HashMap<usize, u64> = HashMap::new();
        let answer = count_within_range(&input, &mut cache, 0);

        println!("{}", answer);
    }
}