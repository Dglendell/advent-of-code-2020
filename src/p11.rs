fn structure_data(p0: Vec<&str>) -> Vec<char> {
    let data: Vec<char> = p0.clone().iter().map(|x: &&str| x.clone().chars().collect::<Vec<char>>()).flatten().collect::<Vec<char>>();

    return data;
}

fn should_change(height: usize, width: usize, data: &Vec<char>, x: usize, y: usize) -> bool {

    if y > height || x > width {
        return false;
    }

    let my_seat = *data.get(y * width + x).unwrap();

    let mut points: Vec<char> = Vec::new();
    if x > 0 {
        let point = *data.get(y * width + x - 1).unwrap();
        points.push(point);
    }
    if x + 1 < width {
        let point = *data.get(y * width + x + 1).unwrap();
        points.push(point);
    }

    if y > 0 {
        let cy = y - 1;
        if x > 0 {
            let point = *data.get(cy * width + x - 1).unwrap();
            points.push(point);
        }

        let point = *data.get(cy * width + x).unwrap();
        points.push(point);

        if x + 1 < width {
            let point = *data.get(cy * width + x + 1).unwrap();
            points.push(point);
        }
    }

    if y + 1 < height {
        let cy = y + 1;
        if x > 0 {
            let point = *data.get(cy * width + x - 1).unwrap();
            points.push(point);
        }

        let point = *data.get(cy * width + x).unwrap();
        points.push(point);

        if x + 1 < width {
            let point = *data.get(cy * width + x + 1).unwrap();
            points.push(point);
        }
    }

    let taken: i32 = points.iter().map(|x| if *x == '#' { 1 } else { 0 }).sum();

    if my_seat == 'L' {
        taken < 1
    } else if my_seat == '#' {
        taken >= 4
    } else {
        false
    }
}

fn frame(height: usize, width: usize, data: &Vec<char>) -> Vec<char> {
    let mut frame : Vec<char> = Vec::new();

    for y in 0..height {
        for x in 0..width {
            let mut point = data.get(y * width + x).unwrap().clone();

            if should_change(height, width, &data.clone(), x, y) {
                if point == 'L' {
                    point = '#';
                } else if point == '#' {
                    point = 'L';
                }
            }

            frame.push(point);
        }
    }

    frame
}

// Only used to help debug
#[allow(dead_code)]
fn render(height: usize, width: usize, data: &Vec<char>) {
    for y in 0..height {
        for x in 0..width {
            let point = *data.get(y * width + x).unwrap();

            print!("{}", point);
        }

        print!("\n");
    }
}

fn compare_frames(_frame_one: &Vec<char>, _frame_two: &Vec<char>) -> bool {
    if _frame_one.len() != _frame_two.len() {
        return false;
    }

    for index in 0.._frame_one.len() {
        let a = _frame_one.get(index).unwrap();
        let b = _frame_two.get(index).unwrap();

        if a != b {
            return false
        }
    }

    true
}

fn find_taken_seats(height: usize, width: usize, data: &Vec<char>) -> i32 {
    let mut current_frame = data.clone();

    loop {
        let next_frame = frame(height, width, &current_frame);

        if compare_frames(&current_frame, &next_frame) {
            break;
        }

        current_frame = next_frame.clone();
    }

    let taken: i32 = current_frame.iter().map(|c| if *c == '#' { 1 } else { 0 }).sum();

    taken
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::helpers::file_to_vec_string;

    #[test]
     fn test_structure_data() {
         let input = vec![
             "L.LL.LL.LL",
         "LLLLLLL.LL",
         "L.L.L..L..",
         "LLLL.LL.LL",
         "L.LL.LL.LL",
         "L.LLLLL.LL",
         "..L.L.....",
         "LLLLLLLLLL",
         "L.LLLLLL.L",
         "L.LLLLL.LL"];

        let _height = input.len();
        let _width = input.first().unwrap().len();
        let data = structure_data(input);

        assert_eq!(*data.get(6*_width+2).unwrap(), 'L');
     }

    #[test]
    fn test_should_change() {
        let input = vec![
            "L.LL.LL.LL",
            "LLLLLLL.LL",
            "L.L.L..L..",
            "LLLL.##.LL",
            "L.LL.##.LL",
            "L.LLLL#.LL",
            "..L.L.....",
            "LLLLLLLLLL",
            "L.LLLLLL.L",
            "L.LLLLL.LL"];

        let _height = input.len();
        let _width = input.first().unwrap().len();
        let data = structure_data(input);

        assert_eq!(should_change(_height, _width, &data, 1, 1), true);
        assert_eq!(should_change(_height, _width, &data, 4, 2), true);
        assert_eq!(should_change(_height, _width, &data, 5, 4), true);
        assert_eq!(should_change(_height, _width, &data, 5, 3), false);
    }

    #[test]
    fn test_frame() {
        let input = vec![
            "L.LL.LL.LL",
            "LLLLLLL.LL",
            "L.L.L..L..",
            "LLLL.LL.LL",
            "L.LL.LL.LL",
            "L.LLLLL.LL",
            "..L.L.....",
            "LLLLLLLLLL",
            "L.LLLLLL.L",
            "L.LLLLL.LL"];

        let _height = input.len();
        let _width = input.first().unwrap().len();
        let data = structure_data(input);

        let _frame = frame(_height, _width, &data);

        let free: i32 = _frame.iter().map(|c| if *c == 'L' { 1 } else { 0 }).sum();
        assert_eq!(free, 0);
    }

    #[test]
    fn test_find_free_seats() {
        let input = vec![
            "L.LL.LL.LL",
            "LLLLLLL.LL",
            "L.L.L..L..",
            "LLLL.LL.LL",
            "L.LL.LL.LL",
            "L.LLLLL.LL",
            "..L.L.....",
            "LLLLLLLLLL",
            "L.LLLLLL.L",
            "L.LLLLL.LL"];

        let _height = input.len();
        let _width = input.first().unwrap().len();
        let data = structure_data(input);

        let seats = find_taken_seats(_height, _width, &data);

        println!("{} seats occupied", seats);
    }

    #[test]
    fn solution_part_1() {
        let raw_input: Vec<String> = file_to_vec_string("inputs/p11.txt");
        let input: Vec<&str> = raw_input.iter().map(|x| x.as_str()).collect();

        let _height = input.len();
        let _width = input.first().unwrap().len();
        let data = structure_data(input);

        let seats = find_taken_seats(_height, _width, &data);

        println!("{} seats occupied", seats);
    }
}