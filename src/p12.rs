struct Ship {
    x: i32,
    y: i32,
    dir: char,
}

impl Ship {
    fn new() -> Ship {
        Ship {
            x: 0,
            y: 0,
            dir: 'E',
        }
    }

    fn execute(&mut self, action: &str) {
        let (instruction, length) = action.split_at(1);
        let length = length.parse::<i32>().expect("length is not an integer");

        match instruction {
            "N" => {
                self.y += length;
            }
            "S" => {
                self.y -= length;
            }
            "E" => {
                self.x += length;
            }
            "W" => {
                self.x -= length;
            }
            "L" => {
                let full_turns = length / 360;
                let turns = (360 - (length - (full_turns * 360))) / 90;

                for _ in 0..turns {
                    self.turn();
                }
            }
            "R" => {
                let turns = length / 90;

                for _ in 0..turns {
                    self.turn();
                }
            }
            "F" => self.mv(length),
            &_ => panic!("Unknown instruction")
        }
    }

    fn turn(&mut self) {
        self.dir = match self.dir {
            'N' => 'E',
            'E' => 'S',
            'S' => 'W',
            'W' => 'N',
            _ => panic!("Unknown direction")
        };
    }

    fn mv(&mut self, length: i32) {
        self.execute(format!("{}{}", self.dir, length).as_str());
    }

    fn manhattan(&self) -> i32 {
        self.x.abs() + self.y.abs()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::helpers::file_to_vec_string;

    #[test]
    fn test_ship() {
        let ship = Ship::new();

        assert_eq!(ship.x, 0);
        assert_eq!(ship.y, 0);
        assert_eq!(ship.dir, 'E');
    }

    #[test]
    fn test_ship_execute() {
        let mut ship = Ship::new();

        assert_eq!(ship.x, 0);
        assert_eq!(ship.y, 0);
        assert_eq!(ship.dir, 'E');

        ship.execute("N10");

        assert_eq!(ship.y, 10);
        assert_eq!(ship.x, 0);
        assert_eq!(ship.dir, 'E');
    }

    #[test]
    fn test_execute_demo() {
        let mut ship = Ship::new();
        let input = vec![
            "F10",
            "N3",
            "F7",
            "R90",
            "F11"
        ];

        for action in input {
            ship.execute(action);
        }

        println!("{} {}", ship.x, ship.y);

        assert_eq!(ship.manhattan(), 25);
    }

    #[test]
    fn solution_part_1() {
        let raw_input = file_to_vec_string("inputs/p12.txt");
        let input: Vec<_> = raw_input.iter().map(|s| s.as_str()).collect();

        let mut ship = Ship::new();

        for action in input {
            ship.execute(action);
        }

        println!("The manhattan distance is {}", ship.manhattan());
    }
}