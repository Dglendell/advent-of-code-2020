struct ShipWithNavigation {
    x: i32,
    y: i32,
    w_x: i32,
    w_y: i32,
    dir: char,
}

impl ShipWithNavigation {
    fn new() -> ShipWithNavigation {
        ShipWithNavigation {
            x: 0,
            y: 0,
            w_x: 10,
            w_y: 1,
            dir: 'E',
        }
    }

    fn execute(&mut self, action: &str) {
        let (instruction, length) = action.split_at(1);
        let length = length.parse::<i32>().expect("length is not an integer");

        match instruction {
            "N" => {
                self.w_y += length;
            }
            "S" => {
                self.w_y -= length;
            }
            "E" => {
                self.w_x += length;
            }
            "W" => {
                self.w_x -= length;
            }
            "L" => {
                let full_turns = length / 360;
                let turns = (360 - (length - (full_turns * 360))) / 90;

                for _ in 0..turns {
                    self.turn();
                }
            }
            "R" => {
                let turns = length / 90;

                for _ in 0..turns {
                    self.turn();
                }
            }
            "F" => self.mv(length),
            &_ => panic!("Unknown instruction")
        }
    }

    fn turn(&mut self) {
        self.dir = match self.dir {
            'N' => 'E',
            'E' => 'S',
            'S' => 'W',
            'W' => 'N',
            _ => panic!("Unknown direction")
        };

        let wx = self.w_x;
        self.w_x = self.w_y;
        self.w_y = -wx;
    }

    fn mv(&mut self, length: i32) {
        self.x += self.w_x * length;
        self.y += self.w_y * length;
    }

    fn manhattan(&self) -> i32 {
        self.x.abs() + self.y.abs()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::helpers::file_to_vec_string;

    #[test]
    fn test_ship() {
        let input: Vec<_> = vec![
            "F10",
            "N3",
            "F7",
            "R90",
            "F11"
        ];

        let mut ship = ShipWithNavigation::new();

        for action in input {
            ship.execute(action);
        }

        println!("{}", ship.manhattan());
    }

    #[test]
    fn solution_part_2() {
        let raw_input = file_to_vec_string("inputs/p12.txt");
        let input: Vec<_> = raw_input.iter().map(|s| s.as_str()).collect();

        let mut ship = ShipWithNavigation::new();

        for action in input {
            ship.execute(action);
        }

        println!("The manhattan distance is {}", ship.manhattan());
    }
}