fn find_route(input: Vec<String>) -> i32 {
    let time = input.get(0)
        .expect("no time")
        .to_string()
        .parse::<i32>()
        .expect("couldn't parse time to int");

    let routes : Vec<_> = input.get(1)
        .expect("no routes")
        .split(',')
        .map(|x| x.parse::<i32>())
        .collect();

    let mut routes_without_x: Vec<_> = routes.iter()
        .filter(|s| s.is_ok())
        .map(|s| s.clone().unwrap())
        .collect();
    routes_without_x.sort();

    let mut min: Option<i32> = None;
    let mut minutes_to_wait: i32 = 0;

    for i in routes_without_x {
        if let Some(x) = min {
            if (i - (time % i)) < (x - (time % x)) {
                min = Some(i);
                minutes_to_wait = i - (time % i);
            }
        } else {
            min = Some(i);
            minutes_to_wait = i - (time % i);
        }
    }

    min.unwrap() * minutes_to_wait
}

#[allow(dead_code)]
fn find_earliest_ts(input: Vec<String>) -> u32 {
    let mut index = 0;

    let routes : Vec<_> = input.get(1)
        .expect("no routes")
        .split(',')
        .map(|x| x.parse::<u32>().unwrap_or(0))
        .collect();

    let mut rem: Option<u32> = None;
    loop {
        index += 1;

        let mut count = 0;
        for route in routes.clone() {
            println!("{}", route);
            if let Some(r) = rem {
                if route == 0 {
                    rem = Some(r + 1);
                    count += 1;
                } else if r + 1 == (route - (index % route)) {
                    rem = Some(r + 1);
                    count += 1;
                }
            } else {
                rem = Some(route - (index % route));
                count += 1;
            }
        }
        println!("{}", count);
        if count == (routes.len() - 1) {
            break;
        }
    }

    println!("{:?}", routes);
    rem.unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::helpers::file_to_vec_string;

    #[test]
    fn test_find_route() {
        let result = find_route(vec![
            "939".to_string(),
            "7,13,x,x,59,x,31,19".to_string()
        ]);

        assert_eq!(result, 295);
    }

    #[test]
    fn solution_part_1() {
        let input = file_to_vec_string("inputs/p13.txt");

        let result = find_route(input);

        println!("Take bus {}", result);
    }

    // TODO - unfinished solution for part 2
    #[allow(dead_code)]
    fn test_find_earliest_ts() {
        let result = find_earliest_ts(vec![
            "939".to_string(),
            "17,x,13,19".to_string()
        ]);

        assert_eq!(result, 1068781);
    }
}