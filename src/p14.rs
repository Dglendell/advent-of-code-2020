use std::collections::HashMap;

fn add_mask(mask: &Vec<char>, value: u64) -> u64 {
    let mut output = value;

    for x in 1..=mask.len() {
        let index = mask.len() - x;

        let ch = *mask.get(index).unwrap();

        if ch == 'X' {
            continue;
        }

        if ch == '1' {
            output |= 1 << (x - 1);
        } else if ch == '0' {
            output &= !(1 << (x - 1));
        }
    }

    output
}

fn run_program(program: &Vec<String>) -> u64 {
    let mut mask: Option<Vec<char>> = None;
    let mut memory: HashMap<u64, u64> = HashMap::new();

    for code in program {
        let (instruction, value) = code.split_at(code.find("=").unwrap() + 1);
        if instruction.contains("mask") {
            mask = Some(value.trim().chars().collect::<Vec<char>>());
            continue;
        }

        if let Some(m) = &mask {
            let address = instruction.trim_start_matches("mem[").trim_end_matches("] =").parse::<u64>().unwrap();
            let int_value = value.trim().parse::<u64>().unwrap();

            memory.insert(address, add_mask(m, int_value));
        }
    }

    memory.values().sum()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::helpers::file_to_vec_string;

    #[test]
    fn test_add_mask() {
        let mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X".chars().collect::<Vec<_>>();

        assert_eq!(add_mask(&mask, 11), 73);
        assert_eq!(add_mask(&mask, 0), 64);
    }

    #[test]
    fn test_run_program() {
        let program = vec![
            "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X".to_string(),
            "mem[8] = 11".to_string(),
            "mem[7] = 101".to_string(),
            "mem[8] = 0".to_string()
        ];

        let result = run_program(&program);

        assert_eq!(result, 165);
    }

    #[test]
    fn solution_part_1() {
        let program = file_to_vec_string("inputs/p14.txt");

        let result = run_program(&program);

        println!("The sum is {}", result);
    }
}