use std::collections::HashMap;

fn memory_game(start_numbers: Vec<u32>, turns: usize) -> u32 {
    let mut memory:  HashMap<u32, usize> = HashMap::new();

    for (index, number) in start_numbers.iter().enumerate() {
        if index == start_numbers.len()-1 {
            break;
        }

        memory.insert(*number, index + 1);
    }

    let mut latest_spoken = *start_numbers.last().unwrap();
    for turn in start_numbers.len()..turns {
        let number = memory.get(&latest_spoken);
        if let Some(index) = number.cloned() {
            memory.insert(latest_spoken, turn);
            latest_spoken = (turn - index) as u32;
        } else {
            memory.insert(latest_spoken, turn);
            latest_spoken = 0;
        }
    }

    latest_spoken
}

#[cfg(test)]
mod tests {
    use crate::p15::memory_game;

    #[test]
    fn test_memory_game() {
        assert_eq!(memory_game(vec![0, 3, 6], 2020), 436);
    }

    #[test]
    fn solution_part_1() {
        let result = memory_game(vec![13,0,10,12,1,5,8], 2020);

        println!("The 2020th number is {}", result);
    }

    #[test]
    fn solution_part_2() {
        let result = memory_game(vec![13,0,10,12,1,5,8], 30_000_000);

        println!("The 2020th number is {}", result);
    }
}