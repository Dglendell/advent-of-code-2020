use std::ops::Range;
use std::collections::HashMap;

fn get_conditions(input: &Vec<String>) -> HashMap<String,Vec<Range<usize>>> {
    let mut data : HashMap<String, Vec<Range<usize>>> = HashMap::new();

    for row in input {
        if row.as_bytes().is_empty() {
            break;
        }

        let (name, ranges) = row.split_at(row.find(":").unwrap());

        let ranges: Vec<_> = ranges
            .trim_start_matches(": ")
            .split(" or ")
            .map(|x| x.split_at(x.find('-').unwrap()))
            .map(|(a, b)| a.parse::<usize>().unwrap()..b.trim_start_matches("-").parse::<usize>().unwrap())
            .collect();

        data.insert(name.to_string(), ranges);
    }

    data
}

fn get_their_tickets(input: &Vec<String>) -> Vec<Vec<usize>> {
    let mut data: Vec<Vec<usize>> = Vec::new();

    let mut should_skip = true;
    for row in input {
        if row.contains("nearby tickets") {
            should_skip = false;
            continue;
        } else if should_skip {
            continue;
        }

        let ticket: Vec<_> = row.split(',').map(|x| x.parse::<usize>().unwrap()).collect();
        data.push(ticket);
    }

    data
}

fn invalid_ticket_numbers(ticket_numbers: Vec<usize>, conditions: &HashMap<String,Vec<Range<usize>>>) -> Vec<usize> {
    let mut invalid_number: Vec<usize> = Vec::new();

    for number in ticket_numbers {
        let mut met_conditions = 0;

        for condition in conditions.values() {
            for r in condition {
                if r.contains(&number) || r.end == number {
                    met_conditions += 1;
                }
            }
        }

        if met_conditions < 1 {
            invalid_number.push(number);
        }

    }

    invalid_number
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::helpers::file_to_vec_string;

    #[test]
    fn test_get_conditions() {
        let input = file_to_vec_string("inputs/p16-sample.txt");
        let conditions = get_conditions(&input);

        let seat = conditions.get("seat").unwrap();
        assert_eq!(seat.get(0).unwrap().start, 13);
        assert_eq!(seat.get(0).unwrap().end, 40);
        assert_eq!(seat.get(1).unwrap().start, 45);
        assert_eq!(seat.get(1).unwrap().end, 50);
    }

    #[test]
    fn test_get_their_tickets() {
        let input = file_to_vec_string("inputs/p16-sample.txt");
        let tickets = get_their_tickets(&input);

        assert_eq!(tickets.len(), 4);

        let first_ticket = tickets.get(0).unwrap();
        assert_eq!(*first_ticket, vec![7,3,47]);
    }

    #[test]
    fn test_validity() {
        let input = file_to_vec_string("inputs/p16-sample.txt");
        let tickets = get_their_tickets(&input);
        let conditions = get_conditions(&input);

        let mut sum = 0;
        for ticket in tickets {
            let invalid_numbers: Vec<usize> = invalid_ticket_numbers(ticket.clone(), &conditions);

            sum += invalid_numbers.iter().fold(0, |acc, x| acc + x);
        }

        assert_eq!(sum, 71);
    }

    #[test]
    fn solution_part_1() {
        let input = file_to_vec_string("inputs/p16.txt");
        let tickets = get_their_tickets(&input);
        let conditions = get_conditions(&input);

        let mut sum = 0;
        for ticket in tickets {
            let invalid_numbers: Vec<usize> = invalid_ticket_numbers(ticket.clone(), &conditions);

            sum += invalid_numbers.iter().fold(0, |acc, x| acc + x);
        }

        println!("{}", sum);
    }
}