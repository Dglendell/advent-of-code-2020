

fn parse(input: Vec<String>) -> Vec<(i32, i32, i32)> {
    let mut coords: Vec<(i32, i32, i32)> = Vec::new();

    for (y, row) in input.iter().enumerate() {
        for (x, c) in row.chars().enumerate() {
            if c == '#' {
                coords.push((x as i32, y as i32, 0));
            }
        }
    }

    coords
}

fn run_once(coords: Vec<(i32, i32, i32)>) -> Vec<(i32, i32, i32)> {
    let mut new_coords: Vec<(i32, i32, i32)> = Vec::new();

    for z in -15..15 {
        for y in -15..15 {
            for x in -15..15 {
                let mut active_neighbours = 0;

                let is_active = coords.contains(&(x, y, z));

                for active in &coords {
                    if (active.0 - x).abs() <= 1 && (active.1 - y).abs() <= 1 && (active.2 - z).abs() <= 1 {
                        if *active != (x, y, z) {
                            active_neighbours += 1;
                        }
                    }
                }

                if is_active && (active_neighbours == 2 || active_neighbours == 3) {
                    new_coords.push((x, y, z));
                } else if !is_active && active_neighbours == 3 {
                    new_coords.push((x, y, z));
                }
            }
        }
    }

    new_coords
}

fn parse_4d(input: Vec<String>) -> Vec<(i32, i32, i32, i32)> {
    let mut coords: Vec<(i32, i32, i32, i32)> = Vec::new();

    for (y, row) in input.iter().enumerate() {
        for (x, c) in row.chars().enumerate() {
            if c == '#' {
                coords.push((x as i32, y as i32, 0, 0));
            }
        }
    }

    coords
}

fn run_once_4d(coords: Vec<(i32, i32, i32, i32)>) -> Vec<(i32, i32, i32, i32)> {
    let mut new_coords: Vec<(i32, i32, i32, i32)> = Vec::new();

    for z in -15..15 {
        for y in -15..15 {
            for x in -15..15 {
                for w in -15..15 {
                    let mut active_neighbours = 0;

                    let is_active = coords.contains(&(x, y, z, w));

                    for active in &coords {
                        if (active.0 - x).abs() <= 1 && (active.1 - y).abs() <= 1 && (active.2 - z).abs() <= 1 && (active.3 - w).abs() <= 1 {
                            if *active != (x, y, z, w) {
                                active_neighbours += 1;
                            }
                        }
                    }

                    if is_active && (active_neighbours == 2 || active_neighbours == 3) {
                        new_coords.push((x, y, z, w));
                    } else if !is_active && active_neighbours == 3 {
                        new_coords.push((x, y, z, w));
                    }
                }
            }
        }
    }

    new_coords
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::helpers::file_to_vec_string;

    #[test]
    fn test_parse() {
        let data = parse(vec![".#.".to_string(), "..#".to_string(), "###".to_string()]);

        println!("{:?}", data);
    }

    #[test]
    fn test_run_once() {
        let data = parse(vec![".#.".to_string(), "..#".to_string(), "###".to_string()]);

        let output = run_once(data);
        println!("{:?}", output);
    }

    #[test]
    fn test_run_once_6x() {
        let mut data = parse(vec![".#.".to_string(), "..#".to_string(), "###".to_string()]);

        for _ in 0..6 {
            data = run_once(data);
        }

        assert_eq!(data.len(), 112);
    }

    #[test]
    fn solution_part_1() {
        let input = file_to_vec_string("inputs/p17.txt");
        let mut data = parse(input);

        for _ in 0..6 {
            data = run_once(data);
        }

        println!("{}", data.len());
    }

    #[test]
    fn solution_part_2() {
        let input = file_to_vec_string("inputs/p17.txt");
        let mut data = parse_4d(input);

        for _ in 0..6 {
            data = run_once_4d(data);
        }

        println!("{}", data.len());
    }
}