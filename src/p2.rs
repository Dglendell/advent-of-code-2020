mod p2 {
    pub fn test_password(input: &str) -> bool {
        let parts: Vec<&str> = input.split_whitespace().collect();
        let ranges = parts[0].split('-').collect::<Vec<_>>();
        let (min, max): (usize, usize) = (ranges[0].parse().expect("not int"), ranges[1].parse().expect("not int"));
        let character: char = parts[1].trim_end_matches(':').chars().next().expect("not a char");

        let number_of_char = parts[2].chars().filter(|c| *c == character).count();

        return number_of_char >= min && number_of_char <= max;
    }

    pub fn test_password_part2(input: &str) -> bool {
        let parts: Vec<&str> = input.split_whitespace().collect();
        let ranges = parts[0].split('-').collect::<Vec<_>>();
        let (first, last): (usize, usize) = (ranges[0].parse().expect("not int"), ranges[1].parse().expect("not int"));
        let character: char = parts[1].trim_end_matches(':').chars().next().expect("not a char");
        let password = parts[2].chars().collect::<Vec<_>>();

        return (character == password[first - 1] || character == password[last - 1]) && password[first - 1] != password[last - 1];
    }
}

#[cfg(test)]
mod tests {
    use super::p2::*;
    use crate::helpers::helpers::file_to_vec_string;

    #[test]
    fn test_test_password() {
        assert_eq!(test_password("1-3 a: abcde"), true);
        assert_eq!(test_password("1-3 b: cdefg"), false);
        assert_eq!(test_password("2-9 c: ccccccccc"), true);
    }

    #[test]
    fn test_test_password_part2() {
        assert_eq!(test_password_part2("1-3 a: abcde"), true);
        assert_eq!(test_password_part2("1-3 b: cdefg"), false);
        assert_eq!(test_password_part2("2-9 c: ccccccccc"), false);
    }

    #[test]
    fn final_test_passwords() {
        let inputs = file_to_vec_string("inputs/p2.txt");

        let result = inputs
            .iter()
            .map(|x| test_password(x.as_str()))
            .filter(|x| *x == true)
            .collect::<Vec<_>>()
            .len();

        println!("There are {} valid passwords", result);

    }

    #[test]
    fn final_test_passwords_part2() {
        let inputs = file_to_vec_string("inputs/p2.txt");

        let result = inputs
            .iter()
            .map(|x| test_password_part2(x.as_str()))
            .filter(|x| *x == true)
            .collect::<Vec<_>>()
            .len();

        println!("There are {} valid passwords", result);

    }
}