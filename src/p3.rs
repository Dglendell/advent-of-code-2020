fn tobboggan_path_tree_count(input: &str, step_x: usize, step_y: usize) -> u32 {
    let map = input
        .split_whitespace()
        .map(|x| x.chars().collect::<Vec<char>>())
        .collect::<Vec<_>>();

    let width = map[0].len();
    let mut y: usize = step_y;
    let mut x: usize = step_x;

    let mut tree_count = 0;

    loop {
        tree_count += if map[y][x % width] == '#' { 1 } else { 0 };

        y += step_y;
        x += step_x;

        if y > (map.len() - 1) {
            break;
        }
    }

    return tree_count;
}

#[cfg(test)]
mod tests {
    use crate::helpers::helpers::file_to_string;
    use crate::p3::tobboggan_path_tree_count;

    #[test]
    fn test_tobboggan_path() {
        let input = "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#";

        assert_eq!(tobboggan_path_tree_count(input, 3, 1), 7);
    }

    #[test]
    fn final_toboggan_path_part1() {
        let input = file_to_string("inputs/p3.txt");

        println!("There are {} trees along the path.",
                 tobboggan_path_tree_count(input.as_str(), 3, 1));
    }

    #[test]
    fn final_toboggan_path_part2() {
        let input = file_to_string("inputs/p3.txt");

        let paths = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
        let mut result: u32 = 1;
        for (x, y) in paths {
            result *= tobboggan_path_tree_count(input.as_str(), x, y);
        }

        println!("The product of the trees along the paths is {}", result);
    }
}