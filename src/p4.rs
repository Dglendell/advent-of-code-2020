use crate::passport::Passport;

fn read_passports(input: &str) -> Vec<String> {
    return input.split("\n\n").map(|s| s.to_string()).collect();
}

fn test_passport_simple(input: String) -> bool {
    let count = input.split_whitespace().count();
    let has_cid = input.contains("cid");

    count == 8 || (count == 7 && !has_cid)
}

fn test_passport_full(input: String) -> bool {
    let passport = Passport::new(input);

    passport.is_valid()
}

#[cfg(test)]
mod tests {
    use crate::p4::{read_passports, test_passport_simple, test_passport_full};
    use crate::helpers::helpers::file_to_string;

    #[test]
    fn test_read_passports() {
        let passports = read_passports("\
ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in");

        assert_eq!(passports.len(), 4);

        assert_eq!(test_passport_simple(passports[0].clone()), true, "field one");
        assert_eq!(test_passport_simple(passports[1].clone()), false, "field two");
        assert_eq!(test_passport_simple(passports[2].clone()), true, "field three");
        assert_eq!(test_passport_simple(passports[3].clone()), false, "field four");
    }

    #[test]
    fn final_count_valid() {
        let input = file_to_string("inputs/p4.txt");

        let passports = read_passports(input.as_str());

        println!("There are {} valid passports.", passports.iter().filter(|p| test_passport_simple((*p).clone())).count())

    }

    #[test]
    fn final_count_full_valid() {
        let input = file_to_string("inputs/p4.txt");

        let passports = read_passports(input.as_str());

        println!("There are {} valid passports.", passports.iter().filter(|p| test_passport_full((*p).clone())).count())
    }
}