fn find_row(input: &str) -> u8 {
    find_by_step(input, 0, 127, 'F', 'B')
}

fn find_by_step(input: &str, min: u8, max: u8, low_char: char, high_char: char) -> u8 {
    let mut min: u8 = min;
    let mut max: u8 = max;
    let mut row: u8 = 0;

    for (index, current_char) in input.char_indices() {
        let is_last = input.rfind(|c| c == low_char || c == high_char).unwrap() == index;

        if current_char == low_char {
            if is_last {
                row = min;
                break;
            } else {
                let step = (max - min + 2) / 2;
                if step > max {
                    max = step - max;
                } else {
                    max = max - step;
                }
            }

        } else if current_char == high_char {
            if is_last {
                row = max;
                break;
            } else {
                let step = (max - min + 2) / 2;
                min = step + min;
            }
        }
    }

    row
}

fn find_column(input: &str) -> u8 {
    find_by_step(input, 0, 7, 'L', 'R')
}

fn calculate_seat_id(input: String) -> u16 {
    let row = find_row(input.as_str()) as u16;
    let column = find_column(input.as_str()) as u16;

    (row * 8) + column
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::helpers::file_to_vec_string;

    #[test]
    fn test_find_row() {
        let row = find_row("FBFBBFFRLR");
        assert_eq!(row, 44);
    }

    #[test]
    fn test_find_column() {
        let column = find_column("FBFBBFFRLR");
        assert_eq!(column, 5);
    }

    #[test]
    fn test_calculate_seat_id() {
        let seat_id = calculate_seat_id("FBFBBFFRLR".to_string());

        assert_eq!(seat_id, 357);

        assert_eq!(calculate_seat_id("BFFFBBFRRR".to_string()), 567);
        assert_eq!(calculate_seat_id("FFFBBBFRRR".to_string()), 119);
        assert_eq!(calculate_seat_id("BBFFBBFRLL".to_string()), 820);
    }

    #[test]
    fn part1_solution() {
        let inputs = file_to_vec_string("inputs/p5.txt");

        let mut highest_seat_id: u16 = 0;
        for input in inputs {
            let seat_id = calculate_seat_id(input);

            if seat_id > highest_seat_id {
                highest_seat_id = seat_id;
            }
        }

        println!("The highest seat id is {}", highest_seat_id);
    }

    #[test]
    fn part2_solution() {
        let inputs = file_to_vec_string("inputs/p5.txt");

        let mut seats: Vec<u16> = Vec::new();
        for input in inputs {
            seats.push(calculate_seat_id(input));
        }
        seats.sort();

        let mut missing: u16 = 0;
        let mut first = *seats.first().unwrap();

        for index in 0..(seats.len() - 1) {
            if seats[index] != (index as u16) + first {
                missing = index as u16 + first;
                first += 1;
            }
        }

        println!("Your seat id is {}", missing);
    }
}