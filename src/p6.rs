use std::collections::HashMap;

fn count_yes_per_group(input: &str) -> u32 {
    let mut different_chars : Vec<char> = Vec::new();

    for letter in input.chars() {
        if !different_chars.contains(&letter) {
            different_chars.push(letter);
        }
    }

    different_chars.len() as u32
}

fn count_and_sum_for_groups(input: String) -> u32 {
    let input: Vec<_> = input.split("\n\n").collect();

    let mut sum: u32 = 0;
    for group in input {
        sum += count_yes_per_group(group.replace("\n", "").as_str());
    }

    sum
}

fn count_all_member_yes_in_group(input: &str) -> u32 {
    let group: Vec<_> = input.trim_end().split('\n').collect();
    let mut letters: HashMap<char, u32> = HashMap::new();

    for person in &group {
        for letter in person.chars() {
            if letters.contains_key(&letter) {
                letters.insert(letter, letters.get(&letter.clone()).unwrap() + 1);
            } else {
                letters.insert(letter, 1);
            }
        }
    }

    let mut ans: u32 = 0;
    for (_letter, count) in letters {
        if count == group.len() as u32 {
            ans += 1;
        }
    }

    ans
}

#[cfg(test)]
mod tests {
    use crate::p6::{count_yes_per_group, count_and_sum_for_groups, count_all_member_yes_in_group};
    use crate::helpers::helpers::file_to_string;

    #[test]
    fn test_count_yes_per_group() {
        let ans = count_yes_per_group("abc");

        assert_eq!(ans, 3);
    }

    #[test]
    fn test_count_and_sum_for_groups() {
        let ans = count_and_sum_for_groups("\
abc

a
b
c

ab
ac

a
a
a
a

b
".to_string());
        assert_eq!(ans, 11);
    }

    #[test]
    fn solution_part_1() {
        let input = file_to_string("inputs/p6.txt");

        let ans = count_and_sum_for_groups(input);

        println!("The sum is {}", ans);
    }

    #[test]
    fn test_count_all_member_yes_in_group() {
        let ans = count_all_member_yes_in_group("\
ab
ac
");
        assert_eq!(ans, 1);
    }


    #[test]
    fn solution_part_2() {
        let input = file_to_string("inputs/p6.txt");

        let mut sum: u32 = 0;
        for group in input.split("\n\n") {
            let ans = count_all_member_yes_in_group(group);

            sum += ans;
        }

        println!("The sum is {}", sum);
    }
}