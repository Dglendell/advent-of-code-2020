use std::collections::{BTreeMap, HashSet, VecDeque};

fn build_parents(input: Vec<String>) -> BTreeMap<String, Vec<(u32, String)>> {
    let mut map : BTreeMap<String, Vec<(u32, String)>> = BTreeMap::new();

    for row in input {
        let data: Vec<_> = row.split("contain").collect();
        let values: Vec<(u32, String)> = data[1]
            .split(", ")
            .map(|s| {
                let out: Vec<_> = s.trim_matches(' ').splitn(2, ' ').collect();
                (out[0].to_string(), out[1].to_string())
            })
            .filter(|(s1, _)| !s1.contains("no"))
            .map(|(s1, s2)| (s1.parse::<u32>().unwrap(), s2.trim_matches('.').trim_end_matches(|c| c == 's').to_string()))
            .map(|(s1, s2)| (s1, s2.to_string()))
            .collect();
        let key = data[0].trim_matches(' ').trim_end_matches('s').to_string();

        for (n, bag) in values {
            if map.contains_key(bag.as_str()) {
                let new_values = map.get_mut(bag.as_str()).unwrap();
                new_values.push((n, key.to_string()));
            } else {
                map.insert(bag, vec![(n, key.to_string())]);
            }
        }
    }

    return map;
}

fn build_containing(input: Vec<String>) -> BTreeMap<String, Vec<(u32, String)>> {
    let mut map : BTreeMap<String, Vec<(u32, String)>> = BTreeMap::new();

    for row in input {
        let data: Vec<_> = row.split("contain").collect();
        let values: Vec<(u32, String)> = data[1]
            .split(", ")
            .map(|s| {
                let out: Vec<_> = s.trim_matches(' ').splitn(2, ' ').collect();
                (out[0].to_string(), out[1].to_string())
            })
            .filter(|(s1, _)| !s1.contains("no"))
            .map(|(s1, s2)| (s1.parse::<u32>().unwrap(), s2.trim_matches('.').trim_end_matches(|c| c == 's').to_string()))
            .map(|(s1, s2)| (s1, s2.to_string()))
            .collect();
        let key = data[0].trim_matches(' ').trim_end_matches('s');

        for (n, bag) in values {
            if map.contains_key(key) {
                let new_values = map.get_mut(key).unwrap();
                new_values.push((n, bag.to_string()));
            } else {
                map.insert(key.to_string(), vec![(n, bag.to_string())]);
            }
        }
    }

    return map;
}

fn count_bag_that_can_contain_bag(bags: BTreeMap<String, Vec<(u32, String)>>) -> u32 {
    let target = "shiny gold bag".to_string();
    let mut seen : HashSet<String> = HashSet::new();

    let mut deque : VecDeque<String> = VecDeque::new();

    deque.push_back(target);
    while !deque.is_empty() {
        let x = deque.pop_front().unwrap();
        if seen.contains(x.as_str()) {
            continue;
        }

        seen.insert(x.clone());
        for y in bags.get(x.as_str()) {
            for (_, bag) in y {
                deque.push_back(bag.to_string());
            }
        }
    }

    (seen.len() - 1) as u32
}

fn price_bag_that_can_contain_bag(target: String, bags: BTreeMap<String, Vec<(u32, String)>>) -> u32 {
    //let target = "shiny gold bag".to_string();
    let mut ans = 1;
    for y in bags.get(target.as_str()) {
        for (n, bag) in y {
            ans += n * price_bag_that_can_contain_bag(bag.to_string(), bags.clone());
        }
    }

    ans
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::helpers::file_to_vec_string;

    #[test]
    fn test_layer() {
        let data = build_parents("\
light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.".split('\n').map(|s| s.to_string()).collect());

        let bag_count = count_bag_that_can_contain_bag(data);
        assert_eq!(bag_count, 4);
    }

    #[test]
    fn test_price() {
        let data = build_containing("\
light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.".split('\n').map(|s| s.to_string()).collect());

        let bag_count = price_bag_that_can_contain_bag("shiny gold bag".to_string(),data);
        assert_eq!(bag_count - 1, 32);
    }

    #[test]
    fn solution_part1() {
        let input = file_to_vec_string("inputs/p7.txt");

        let bags = build_parents(input);

        let bag_count = count_bag_that_can_contain_bag(bags);

        println!("Find {} bags that can contain shiny gold bag.", bag_count);
    }

    #[test]
    fn solution_part2() {
        let input = file_to_vec_string("inputs/p7.txt");

        let bags = build_containing(input);

        let bag_count = price_bag_that_can_contain_bag("shiny gold bag".to_string(), bags);

        println!("Price is {}", bag_count - 1);
    }
}