use std::collections::HashSet;

fn format_program(input: Vec<String>) -> Vec<(String, i32)> {
    let mut program : Vec<(String, i32)> = Vec::new();

    for code_row in input {
        let code: Vec<_> = code_row.split(' ').collect();

        let instruction = code[0].to_string();
        let op1 = code[1].parse::<i32>().unwrap();
        program.push((instruction, op1));
    }

    program
}

fn run_boot(program: Vec<(String, i32)>, stop_on_duplicate: bool) -> Option<i32> {
    let mut has_run: HashSet<i32> = HashSet::new();

    let mut index: i32 = 0;
    let mut acc: i32 = 0;
    let mut trie: i32 = 0;

    while (index as usize) < program.len() && trie < 1000 {
        trie += 1;

        if has_run.contains(&index) && stop_on_duplicate {
            break;
        }

        let data = program.get(index as usize);
        if data.is_none() {
            break;
        }

        let (instruction, op1) = data.unwrap();
        has_run.insert(index);

        match instruction.as_str() {
            "nop" => {
                index += 1;
            }
            "acc" => {
                acc += op1;
                index += 1;
            }
            "jmp" => {
                index += op1;
            }
            _ => {
                println!("Arrived at unknown instruction {}", instruction);
                break;
            }
        }

    }

    if (index as usize) >= program.len() || stop_on_duplicate {
        Some(acc)
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::helpers::file_to_vec_string;

    #[test]
    fn run_boot_test() {
        let program = format_program(vec![
            "nop +0".to_string(),
            "acc +1".to_string(),
            "jmp +4".to_string(),
            "acc +3".to_string(),
            "jmp -3".to_string(),
            "acc -99".to_string(),
            "acc +1".to_string(),
            "jmp -4".to_string(),
            "acc +6".to_string()
        ]);

        let answ = run_boot(program, true);

        assert_eq!(answ.unwrap(), 5);
    }

    #[test]
    fn find_solution_part1() {
        let input = file_to_vec_string("inputs/p8.txt");

        let program = format_program(input);
        let answer = run_boot(program, true);

        println!("The accumulator is {}", answer.unwrap());
    }

    #[test]
    fn find_solution_part2() {
        let input = file_to_vec_string("inputs/p8.txt");

        let program: Vec<(String, i32)> = format_program(input);
        for index in 0..program.len() {
            let mut new_program: Vec<(String, i32)> = program.clone();


            let row = new_program.get(index).unwrap().clone();
            if row.0 == "nop" {
                std::mem::replace(&mut new_program[index], ("jmp".to_string(), row.1));
            } else if row.0 == "jmp" {
                std::mem::replace(&mut new_program[index], ("nop".to_string(), row.1));
            } else {
                continue;
            }

            if let Some(answer) = run_boot(new_program, false) {
                println!("Found {}", answer);
                break;
            }
        }
    }
}