use std::collections::{HashSet, VecDeque};

fn find_sum_in_numbers(numbers: Vec<i64>, preamble_size: i64) -> Option<i64> {
    let numbers = numbers.clone();
    let mut invalid: Option<i64> = None;

    let mut preamble: VecDeque<i64> = VecDeque::new();
    let mut index = 0;
    while invalid == None {

        if index < preamble_size {
            preamble.push_front(numbers.get(index as usize).unwrap().clone());
            index += 1;
            continue;
        }

        let next = numbers.get(index as usize).unwrap().clone();
        index += 1;

        if !has_sum_in_preamble(Vec::from(preamble.clone()), next) {
            invalid = Some(next);
        } else {
            preamble.pop_back();
            preamble.push_front(next);
        }
    }

    invalid
}

fn has_sum_in_preamble(preamble: Vec<i64>, sum: i64) -> bool {
    let mut preamble = preamble.clone();

    let mut sums: HashSet<i64> = HashSet::new();

    for _ in 0..preamble.len() {
        let left = preamble.pop().unwrap().clone();

        for right in preamble.clone() {
            sums.insert(left + right);
        }
    }

    sums.contains(&sum)
}

fn find_adjecent_sum(numbers: Vec<i64>, find: i64) -> Vec<i64> {
    let mut window: VecDeque<i64> = VecDeque::new();

    let mut sum: i64 = 0;
    let mut index = 0;
    let mut window_index = 0;
    while index < numbers.len() {
        let ri = index + window_index;

        let next = numbers.get(ri).unwrap().clone();
        window_index += 1;

        window.push_back(next);
        sum += next;
        if find == sum {
            break;
        } else if sum > find {
            window.clear();
            sum = 0;
            window_index = 0;
            index += 1;
        }
    }

    Vec::from(window)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::helpers::helpers::file_to_vec_i64;

    #[test]
    fn test_has_sum_in_preamble() {
        let found = has_sum_in_preamble(vec![35,
20,
15,
25,
47], 40);

        assert_eq!(found, true);
    }

    #[test]
    fn test_find_sum_in_numbers() {
        let number = find_sum_in_numbers(vec![35,
20,
15,
25,
47,
40,
62,
55,
65,
95,
102,
117,
150,
182,
127,
219,
299,
277,
309,
576], 5);

        assert_eq!(number, Some(127));
    }

    #[test]
    fn find_solution_part1() {
        let input = file_to_vec_i64("inputs/p9.txt");

        let number = find_sum_in_numbers(input, 25);

        println!("First match is {}", number.expect("Couldn't find number"));
    }

    #[test]
    fn test_find_adjecent_sum() {
        let number = find_adjecent_sum(vec![35,
                                              20,
                                              15,
                                              25,
                                              47,
                                              40,
                                              62,
                                              55,
                                              65,
                                              95,
                                              102,
                                              117,
                                              150,
                                              182,
                                              127,
                                              219,
                                              299,
                                              277,
                                              309,
                                              576], 127);

        assert_eq!(number, vec![15, 25, 47, 40]);
    }

    #[test]
    fn find_solution_part2() {
        let input = file_to_vec_i64("inputs/p9.txt");

        let number = find_sum_in_numbers(input.clone(), 25).unwrap();

        let mut answer = find_adjecent_sum(input.clone(), number);
        answer.sort();
        println!("The key is {}", answer.first().unwrap() + answer.last().unwrap());
    }
}