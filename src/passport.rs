use std::collections::HashMap;
use regex::Regex;

#[derive(Eq, PartialEq, Debug)]
pub struct Passport {
    byr: u32,
    iyr: u32,
    eyr: u32,
    hgt: String,
    hcl: String,
    ecl: String,
    pid: String,
    cid: String
}

impl Passport {
    pub fn new(input: String) -> Passport {
        let fields: Vec<_> = input.split_whitespace().collect();
        let mut output: HashMap<String, String> = HashMap::new();

        for field in fields {
            let (key, value) = field.split_at(4);

            output.insert(key.trim_end_matches(':').to_string(), value.to_string());
        }
        Passport {
            byr: output.get("byr").unwrap_or(&"0".to_string()).parse().unwrap(),
            iyr: output.get("iyr").unwrap_or(&"0".to_string()).parse().unwrap(),
            eyr: output.get("eyr").unwrap_or(&"0".to_string()).parse().unwrap(),
            hgt: output.get("hgt").unwrap_or(&"".to_string()).clone(),
            hcl: output.get("hcl").unwrap_or(&"".to_string()).clone(),
            ecl: output.get("ecl").unwrap_or(&"".to_string()).clone(),
            pid: output.get("pid").unwrap_or(&"".to_string()).clone(),
            cid: output.get("cid").unwrap_or(&"".to_string()).clone(),
        }
    }

    pub fn is_valid(&self) -> bool {
        let byr_valid = self.byr >= 1920 && self.byr <= 2002;
        let iyr_valid = self.iyr >= 2010 && self.iyr <= 2020;
        let eyr_valid = self.eyr >= 2020 && self.eyr <= 2030;

        let hgt_valid = self.is_hgt_valid();
        let hcl_valid = self.is_hcl_valid();
        let ecl_valid = self.is_ecl_valid();
        let pid_valid = self.is_pid_valid();

        byr_valid && iyr_valid && eyr_valid && hgt_valid && hcl_valid && ecl_valid && pid_valid
    }

    fn is_hgt_valid(&self) -> bool {
        if self.hgt.contains("cm") {
            let hgt = self.hgt.trim_end_matches("cm").parse().unwrap_or(0);

            return hgt >= 150 && hgt <= 193;
        }

        if self.hgt.contains("in") {
            let hgt = self.hgt.trim_end_matches("in").parse().unwrap_or(0);

            return hgt >= 59 && hgt <= 76;
        }

        false
    }

    fn is_hcl_valid(&self) -> bool {
        lazy_static! {
            static ref RE: Regex = Regex::new("^#[a-f0-9]{6}$").unwrap();
        }

        RE.is_match(self.hcl.as_str())
    }

    fn is_ecl_valid(&self) -> bool {
        lazy_static! {
            static ref RE: Regex = Regex::new("^amb|blu|brn|gry|grn|hzl|oth$").unwrap();
        }

        RE.is_match(self.ecl.as_str())
    }

    fn is_pid_valid(&self) -> bool {
        lazy_static! {
            static ref RE: Regex = Regex::new("^[0-9]{9}$").unwrap();
        }

        RE.is_match(self.pid.as_str())
    }
}